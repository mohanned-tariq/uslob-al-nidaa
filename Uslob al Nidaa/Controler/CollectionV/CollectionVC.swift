//
//  CollectionVC.swift
//  Uslob al Nidaa
//
//  Created by Mohanned on 1/30/20.
//  Copyright © 2020 Horouf. All rights reserved.
//

import UIKit
import AVFoundation
class CollectionVC: UIViewController {
    
    @IBOutlet weak var questionCollectionView: UICollectionView!
    @IBOutlet weak var labl: UILabel!
    //  var sentence : String = "قَرطُوس سَلِّم على الأَصْدِقَاءِ لِنَبدَأَ اللُّعبَةَ"
    var sentence = "اقرطوس سلم علي الآصدقاء لنبدآ اللعبه"
    var editedSectence = ""
    var cellpic: [UIImage] = [#imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "Joystick"), #imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "Background"), #imageLiteral(resourceName: "Haroof_Gamer") , #imageLiteral(resourceName: "Joystick")]
    
    var iscolEditing: Bool = false
    var longPress: UILongPressGestureRecognizer!
    //    var words = ["أَقَرطُوس", "سَلِّم", "على", "الأَصْدِقَاءِ", "لِنَبدَأَ", "اللُّعبَةَ"]
   // var words = ["الخاص بها", "الخرائط", "تطبيق","من"]
    var words = ["اقرطوس","سلم","علي","الآصدقاء","لنبدآ","اللعبه"]
    var sertedArray : [String] = []
    //word joined
    //    var arrSpeechCountjoined = ["محمد", "Two", "Three", "Four", "Five", "Six", "Seven","eight", "nine", "ten"].joined(separator:" ")
    //
    ///
    var playerLayer: AVPlayerLayer!
    
    var player: AVPlayer?
    @IBOutlet weak var hert1: UIImageView!
    ///
    @IBAction func nextLayout(_ sender: Any) {
        let x = words.reversed().joined(separator:" ")
        
        if x == sentence {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CollectionVC1") as? CollectionVC1
            self.navigationController?.pushViewController(vc!, animated: true)
        } else{
            let alert = UIAlertController(
                title: "خطآ",
                message: "رتب الكلمات آولا ثم انتقل الي السؤال التالي",
                preferredStyle: .alert)
            
            let dismissBtn = UIAlertAction(title: "موافق",
                                           style: .destructive,
                                           handler: nil)
            alert.addAction(dismissBtn)
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionCollectionView.delegate = self
        questionCollectionView.dataSource = self
        
        longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        questionCollectionView.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       playVideos()
    }
    
    
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedItemIndexpath = questionCollectionView.indexPathForItem(at: gesture.location(in: questionCollectionView)) else { return }
            questionCollectionView.beginInteractiveMovementForItem(at: selectedItemIndexpath)
        case .changed:
            questionCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: questionCollectionView))
        case .ended:
            questionCollectionView.endInteractiveMovement()
            
        default:
            questionCollectionView.cancelInteractiveMovement()
        }
    }
    
//    // Action - if mated = true -- else alert
//    @IBAction func speechBtn(_ sender: UIButton) {
//        //print(words, "WORDS")
//        // shelt for mn hena
//        let lang: String = "ar-SA"
//        self.readMe( sentence: sentence , myLang: lang)
//        //        if sender.isSelected {
//        //            Start()
//        //        }else {
//        //            Pause()
//        //        }
//    }
    
        
        //11
        // Action - if mated = true -- else alert
   @IBAction func speechBtn(_ sender: UIButton) {
   let lang: String = "ar-SA"
   self.readMe( sentence: sentence , myLang: lang)
                                    //        if sender.isSelected {
                                    //            Start()
                                    //        }else {
                                    //            Pause()
                                    //        }
                                }
    
    @IBAction func editButton(_ sender: UIBarButtonItem) {
        iscolEditing = !iscolEditing
        sender.title = iscolEditing ? "تم" : "رتب"
        print(words, "WORDS")
        ///////////////////////////////////////////////////
        //العربيه
        let x = words.reversed().joined(separator:" ")
        
        if x == sentence {
            let lang: String = "ar-SA"
            self.readMe( sentence: sentence , myLang: lang)
            //        if sender.isSelected {
            //            Start()
            //        }else {
            //            Pause()
            //        }
            print(sentence)
            labl.text = x
            hert1.isHidden = false
        } else{
            
            let alert = UIAlertController(
                title: "خطآ",
                message: "ترتيب خاطىء .حاول مره آخري",
                preferredStyle: .alert)
            
            let dismissBtn = UIAlertAction(title: "موافق",
                                           style: .destructive,
                                           handler: nil)
            alert.addAction(dismissBtn)
            present(alert, animated: true, completion: nil)
            
        }
//        for (index,word) in words.enumerated(){
//            if index == words.count - 1 {
//                editedSectence.append(word)
//            }else {
//                editedSectence.append(word + " ")
//            }
//
//        }
        print(x)
    }
    
    
//  //  btn 3ady
//        @IBAction func EditButton(_ sender: UIBarButtonItem) {
//            iscolEditing = !iscolEditing
//            sender.settitle = iscolEditing ? "Done" : "Edit"
//        }
    
    //AVSpeechUtteranc
    func readMe( sentence: String , myLang : String) {
        let utterance = AVSpeechUtterance(string: sentence )
        utterance.voice = AVSpeechSynthesisVoice(language: myLang)
        utterance.rate = 0.5
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
    private func playVideos() {
        guard let videoPath = Bundle.main.path(forResource: "VO", ofType:"aiff") else
        {
            fatalError("can't find intro.mp4 file")
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: videoPath))
        player?.play()
    }
}
extension CollectionVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let country = words.remove(at: sourceIndexPath.item)
        words.insert(country, at: destinationIndexPath.item)
    }
    
}
extension CollectionVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        ///////////// here
        return words.count
        
    }
    func numberOfrowinSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sentinceCollectionViewCell", for: indexPath) as? sentinceCollectionViewCell {
            cell.namelabel.text = words[indexPath.item]
           // cell.imageBG.image = cellpic[indexPath.item] ///
            //here gep el data mnha
            cell.deleteClosure = {
                self.words.remove(at: indexPath.item)
                
                
                self.questionCollectionView.reloadData()
            }
            if indexPath.item % 2 == 0 {
                cell.imageBG.image = #imageLiteral(resourceName: "9")
            }else {
               // indexPath.item % 2 == 1
                cell.imageBG.image = #imageLiteral(resourceName: "2")
            }
            
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
extension CollectionVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: (view.frame.width / 3) - 15, height: 100)
        return cellSize
        
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//
//        let seconds = 14.0
//        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//            // Put your code which should be executed with a delay here
//            self.present()
//
//        }
//    }
    
//    func present() {
//        let VC = (storyboard?.instantiateViewController(withIdentifier: "NavigationController") as? NavigationController)!
//        player?.pause()         ////////////////
//        present(VC, animated: true)
//    }

}
