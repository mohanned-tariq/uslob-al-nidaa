//
//  ViewController.swift
//  Uslob al Nidaa
//
//  Created by Mohanned on 1/30/20.
//  Copyright © 2020 Horouf. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playVideos()
        
    }
    var playerLayer: AVPlayerLayer!
    
    var player: AVPlayer?
    //    var playerItem: AVPlayerItem!
    //    var timeObserverToken: Any?
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        self.imageView.image = PPSwiftGifs.animatedImageWithGIFNamed(name: "horouf2")
        
        //   sleep(6)
        
        let seconds = 12.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            // Put your code which should be executed with a delay here
            self.present()
            
        }
    }
    
    func present() {
        let VC = (storyboard?.instantiateViewController(withIdentifier: "NavigationController") as? NavigationController)!
        player?.pause()         ////////////////
        present(VC, animated: true)
    }
    ///vid///
    private func playVideos() {
        guard let videoPath = Bundle.main.path(forResource: "Music 1", ofType:"aiff") else {
            fatalError("can't find intro.mp3 file")
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: videoPath))
        player?.play()

    //    func addTimeobserver(){
    //        let interval = CMTimeRangeMake(start: CMTime, duration: 5)
    //    }
    /////
    //    func addPeriodicTimeObserver() {
    //        // Notify every half second
    //        let timeScale = CMTimeScale(NSEC_PER_SEC)
    //        let time = CMTime(seconds: 5.5, preferredTimescale: timeScale)
    //
    //        timeObserverToken = player.addPeriodicTimeObserver(forInterval: time,
    //                                                           queue: .main) {
    //                                                            [weak self] time in
    //                                                            // update player transport UI
    //        }
    //    }
    
    //    func removePeriodicTimeObserver() {
    //        if let timeObserverToken = timeObserverToken {
    //            player.removeTimeObserver(timeObserverToken)
    //            self.timeObserverToken = nil
    //        }
    //    }
    
   
        
     //    player = AVPlayer(url: URL(fileURLWithPath: videoPath))
        //playerLayer = AVPlayerLayer(player: player)
        // playerLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //playerLayer.videoGravity = .resize
        // playerLayer.zPosition = -1
        // self.videoContainerView.layer.addSublayer(playerLayer)
        
    //    player?.play()
        
        //                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
        //  self.showMainController()
    }
}

///
//        override func viewDidLayoutSubviews() {
//            super.viewDidLayoutSubviews()
//
//            playerLayer.frame = videoContainerView.frame
//        }
//
//        deinit {
//            NotificationCenter.default.removeObserver(self)
//        }

//        private func showMainController() {
//            guard let window = UIApplication.shared.keyWindow else { return }
//
//            let mainController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "main")
//            window.rootViewController = mainController
//            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
//        }


//To call or execute function after some time(After 5 sec)

