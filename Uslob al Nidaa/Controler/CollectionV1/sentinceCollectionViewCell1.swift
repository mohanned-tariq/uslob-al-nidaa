//
//  sentinceCollectionViewCell.swift
//  Uslob al Nidaa
//
//  Created by Mohanned on 1/30/20.
//  Copyright © 2020 Horouf. All rights reserved.
//

import UIKit

class sentinceCollectionViewCell1: UICollectionViewCell {
    var deleteClosure: (()->())?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var imageBG: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 20
      //  containerView.layer.borderWidth = 3
       // containerView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

        containerView.clipsToBounds = true
    }
    @IBAction func deleteButtonPressed(_ sender: Any) {
        deleteClosure?()
    }
}
