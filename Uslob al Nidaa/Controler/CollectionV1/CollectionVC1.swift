//
//  CollectionVC.swift
//  Uslob al Nidaa
//
//  Created by Mohanned on 1/30/20.
//  Copyright © 2020 Horouf. All rights reserved.
//

import UIKit
import AVFoundation
class CollectionVC1: UIViewController {
    
    @IBOutlet weak var questionCollectionView: UICollectionView!
    @IBOutlet weak var labl: UILabel!
    //  var sentence : String = "قَرطُوس سَلِّم على الأَصْدِقَاءِ لِنَبدَأَ اللُّعبَةَ"
    var sentence = "من تطبيق الخرائط الخاص بها"
    var editedSectence = ""
    var cellpic: [UIImage] = [#imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "Joystick"), #imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "Background"), #imageLiteral(resourceName: "Haroof_Gamer") , #imageLiteral(resourceName: "Joystick")]
    
    var iscolEditing: Bool = false
    var longPress: UILongPressGestureRecognizer!
    //    var words = ["أَقَرطُوس", "سَلِّم", "على", "الأَصْدِقَاءِ", "لِنَبدَأَ", "اللُّعبَةَ"]
    var words = ["الخاص بها", "الخرائط", "تطبيق","من"]
    var sertedArray : [String] = []
    //word joined
    //    var arrSpeechCountjoined = ["محمد", "Two", "Three", "Four", "Five", "Six", "Seven","eight", "nine", "ten"].joined(separator:" ")
    //
    ///
    var playerLayer: AVPlayerLayer!
    
    var player: AVPlayer?
    @IBOutlet weak var hert2: UIImageView!
    ///
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionCollectionView.delegate = self
        questionCollectionView.dataSource = self
        
        longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        questionCollectionView.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       playVideos()
    }
    
    
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedItemIndexpath = questionCollectionView.indexPathForItem(at: gesture.location(in: questionCollectionView)) else { return }
            questionCollectionView.beginInteractiveMovementForItem(at: selectedItemIndexpath)
        case .changed:
            questionCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: questionCollectionView))
        case .ended:
            questionCollectionView.endInteractiveMovement()
            
        default:
            questionCollectionView.cancelInteractiveMovement()
        }
    }
    
        
        //11
        // Action - if mated = true -- else alert
    @IBAction func speechBtn(_ sender: UIButton) {
        let lang: String = "ar-SA"
        self.readMe( sentence: sentence , myLang: lang)
        //        if sender.isSelected {
        //            Start()
        //        }else {
        //            Pause()
        //        }
    }
    
    @IBAction func editButton(_ sender: UIBarButtonItem) {
        iscolEditing = !iscolEditing
        sender.title = iscolEditing ? "تم" : "رتب"
        print(words, "WORDS")
        ///////////////////////////////////////////////////
        //العربيه
        let x = words.reversed().joined(separator:" ")
        
        if x == sentence {
            let lang: String = "ar-SA"
            self.readMe( sentence: sentence , myLang: lang)
            //        if sender.isSelected {
            //            Start()
            //        }else {
            //            Pause()
            //        }
            print(sentence)        //2 nafz
            labl.text = x          //3 nafz
            hert2.isHidden = false //4 nafz
        } else{
            
            let alert = UIAlertController(
                title: "خطآ",
                message: "ترتيب خاطىء .حاول مره آخري",
                preferredStyle: .alert)
            
            let dismissBtn = UIAlertAction(title: "موافق",
                                           style: .destructive,
                                           handler: nil)
            alert.addAction(dismissBtn)
            present(alert, animated: true, completion: nil)
            
        }
        print(x)
    }
    
    
//  //  btn 3ady
//        @IBAction func EditButton(_ sender: UIBarButtonItem) {
//            iscolEditing = !iscolEditing
//            sender.settitle = iscolEditing ? "Done" : "Edit"
//        }
    
    //AVSpeechUtteranc
    func readMe( sentence: String , myLang : String) {
        let utterance = AVSpeechUtterance(string: sentence )
        utterance.voice = AVSpeechSynthesisVoice(language: myLang)
        utterance.rate = 0.5
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
    private func playVideos() {
        guard let videoPath = Bundle.main.path(forResource: "VO", ofType:"aiff") else
        {
            fatalError("can't find intro.mp4 file")
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: videoPath))
        player?.play()
    }
}
extension CollectionVC1: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let country = words.remove(at: sourceIndexPath.item)
        words.insert(country, at: destinationIndexPath.item)
    }
    
}
extension CollectionVC1: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        ///////////// here
        return words.count
        
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sentinceCollectionViewCell1", for: indexPath) as? sentinceCollectionViewCell1 {
            cell.namelabel.text = words[indexPath.item]
           // cell.imageBG.image = cellpic[indexPath.item] ///
            //here gep el data mnha
            cell.deleteClosure = {
                self.words.remove(at: indexPath.item)
                
                
                self.questionCollectionView.reloadData()
            }
            if indexPath.item % 2 == 0 {
                cell.imageBG.image = #imageLiteral(resourceName: "1")
            }else {
               // indexPath.item % 2 == 1
                cell.imageBG.image = #imageLiteral(resourceName: "3")
            }
            
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
extension CollectionVC1: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: (view.frame.width / 3) - 15, height: 100)
        return cellSize
        
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//
//        let seconds = 14.0
//        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//            // Put your code which should be executed with a delay here
//            self.present()
//
//        }
//    }
    
}
